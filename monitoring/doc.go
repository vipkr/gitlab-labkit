/*
Package monitoring provides a monitoring endpoint and Continuous Profiling
for Go service processes.

- The monitoring endpoint will expose OpenMetrics at `/metrics`.

- Pprof service endpoints at `/debug/pprof`. More information about the pprof
	endpoints is available at https://golang.org/pkg/net/http/pprof/

- Support the gitlab_build_info metric for exposing application version information

This package includes optional support for continuous profiling, which is responsible for
heap and cpu profiling of the Go process. Currently, the only supported driver is Google
Cloud Platform's Stackdriver Profiler (see https://cloud.google.com/profiler/).

The profiler is initialized upon `monitoring.Start` call.

*Compiling applications with Profiler support*

For compiling binaries with Stackdriver Profiler support (including its dependencies),
build your binary with the `continuous_profiler_stackdriver` compilation tag:

* `go build -tags="continuous_profiler_stackdriver"``

*Initializing the Profiler*

Use the following pattern in the `GITLAB_CONTINUOUS_PROFILING` environment variable
for getting it started:

* `GITLAB_CONTINUOUS_PROFILING="stackdriver?service=gitaly&service_version=1.0.1&project_id=test-123"`

For more information about each argument see https://godoc.org/cloud.google.com/go/profiler#Config.
Most of these shouldn't be required in GCP environments, so a simpler version could be used:

* `GITLAB_CONTINUOUS_PROFILING="stackdriver"`

See https://cloud.google.com/docs/authentication/production#providing_credentials_to_your_application
for authentication details.

*Profiler overhead*

Google Cloud claim Stackdriver Profiler adds a 5% performance overhead to
processes when it is enabled. This cost is only incurred when the profiler is switched on (through
the `GITLAB_CONTINUOUS_PROFILING`) environment variable. More details can be found at
https://medium.com/google-cloud/continuous-profiling-of-go-programs-96d4416af77b.

*/
package monitoring
