#!/usr/bin/env bash

set -eo pipefail

repo=$1
cloneDir=$(mktemp -d)
CI_COMMIT_SHA=${CI_COMMIT_SHA:-master}

git clone "$repo" "$cloneDir"
cd "$cloneDir"

if [ -n "$CI_MERGE_REQUEST_SOURCE_BRANCH_SHA" ]; then
  go get -u=patch gitlab.com/gitlab-org/labkit@"$CI_MERGE_REQUEST_SOURCE_BRANCH_SHA"
else
  go get -u=patch gitlab.com/gitlab-org/labkit@"$CI_COMMIT_SHA"
fi

make

rm -rf "$cloneDir"
